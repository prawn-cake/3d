# -*- coding: utf-8 -*-
"""
Copyright © 2014 Daria Budkina <littlepresent@yandex.ru">

Main server application file for the 3DS Max online tutorial application.
"""
from flask import Flask, render_template


app = Flask(__name__)


@app.route('/')
def main():
    return render_template('main.html')


@app.route('/lessons')
@app.route('/lessons/')
@app.route('/lessons/<int:num>')
def lessons(num=None):
    num = num or 1
    ctx_params = {'lesson_num': num}
    ctx_params['lesson_content'] = render_template(
        'lessons/lesson{}.html'.format(num))
    ctx_params['lesson_num'] = num
    return render_template(
        'lessons.html',
        **ctx_params)


@app.route('/library')
def library():
    return render_template('library.html')


@app.route('/extra')
def extra():
    return render_template('extra.html')


@app.route('/finished_works')
def finished_works():
    return render_template('finished_works.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=False)
