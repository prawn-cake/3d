ENV=$(CURDIR)/.env
BIN=$(ENV)/bin
PYTHON=$(BIN)/python
PYVERSION=$(shell $(PYTHON) -c "import sys; print('python{}.{}'.format(sys.version_info.major, sys.version_info.minor))")
PROJECT=3d


.PHONY: help
# target: help - Display callable targets
help:
	@egrep "^# target:" [Mm]akefile | sed -e 's/^# target: //g'

env: requirements.txt
	[ -d $(ENV) ] || virtualenv --no-site-packages $(ENV)
	$(ENV)/bin/pip install -r requirements.txt
	touch $(ENV)

.PHONY: run
# target: run - Run server
run: env
	$(PYTHON) 3d.py